import './App.css';
import React, { useState } from 'react';
import axios from 'axios'
import env from "react-dotenv";

function App() {

  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');
  const [email, setEmail] = useState('');

  const sendContact = async () => {
    const payload = {
      name: name,
      phone: phone,
      email: email
    }

    if (
      payload.name === ''
      || payload.email === ''
      || payload.phone === ''
    ) {
      alert("A field(s) is missing")
    } else {
      try {
        const res = await axios.post(
          env.API_URL, // HOOK URL
          payload
        )
        const data = res.data;
        console.log(data)
        alert(res.data.success)
        clear()
      } catch (e) {
        console.log(e)
      }
    }
  }

  const clear = () => {
    setName('')
    setPhone('')
    setEmail('')
  }

  return (
    <div className="App">
       <video autoPlay muted loop id="myVideo">
        <source src="/background.mp4" type="video/mp4"></source>
      </video>
      <div className="container content">
        <div className="row">
          <div className="col-sm-6" id="first">
            <div style={{
              width: "32rem",
              height: "32rem",
              backgroundColor: "#6261A3",
              borderRadius: "5px",
              margin: "auto",
              paddingTop: "15px"
            }}>
              <div style={{
                width: "30rem",
                height: "30rem",
                backgroundColor: "#0C0D2C",
                borderRadius: "5px",
                margin: "auto",
                paddingTop: "15px"
              }}>
                <div className="text-center" style={{
                  width: "28rem",
                  height: "5rem",
                  backgroundColor: "#1D1D49",
                  borderRadius: "5px",
                  margin: "auto",
                  padding: "20px",
                  fontSize: "30px",
                  fontFamily: 'Roboto'
                }}>
                  Join Our Mailing List
                </div>

                <div className="mt-3 pt-4" style={{
                  width: "28rem",
                  height: "17rem",
                  backgroundColor: "#1D1D49",
                  borderRadius: "5px",
                  margin: "auto"
                }}>
                  <div>
                    <input
                    type="text"
                    className="form-control"
                    placeholder='Enter first and last name'
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                    style={{
                      border: 1,
                      outline: "none",
                      width: "26rem",
                      fontSize: "20px",
                      height: "4rem",
                      color: "white",
                      backgroundColor: "#0E0E2A",
                      borderRadius: "5px",
                      margin: "auto"
                    }} />
                  </div>
                  <div className='pt-3'>
                    <input
                    type="tel"
                    className="form-control"
                    placeholder='Enter phone number'
                    value={phone}
                    onChange={(e) => setPhone(e.target.value)}
                    style={{
                      border: 1,
                      outline: "none",
                      width: "26rem",
                      fontSize: "20px",
                      height: "4rem",
                      color: "white",
                      backgroundColor: "#0E0E2A",
                      borderRadius: "5px",
                      margin: "auto"
                    }} />
                  </div>
                  <div className='pt-3'>
                    <input
                    type="email"
                    className="form-control"
                    placeholder='Enter an email address'
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    style={{
                      border: 1,
                      outline: "none",
                      width: "26rem",
                      fontSize: "20px",
                      height: "4rem",
                      color: "white",
                      backgroundColor: "#0E0E2A",
                      borderRadius: "5px",
                      margin: "auto"
                    }} />
                  </div>
                </div>
                <div className="mt-3 text-center">
                  <button
                  onClick={() => sendContact()} 
                  type="button"
                  className="btn"
                  style={{
                    width: "28rem",
                    fontSize: "30px",
                    height: "4rem",
                    color: "#fff",
                    fontFamily: 'Roboto',
                    backgroundColor: "#0E0E2A",
                    borderRadius: "5px",
                    margin: "auto",
                    backgroundImage: "linear-gradient(to right, #4F49AA, #349083)"
                  }}>
                    Submit
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="col-sm-6" id="second" style={{
            margin: "auto"
          }}>
              <h5 style={{ color: "#99A2B3", fontFamily: 'Roboto' }}>
                Built with <b><font color="#40BFA0">SKRUMIFY</font></b>
              </h5>
              <h1 style={{ fontFamily: 'Roboto' }}>
                <b>Build landing pages<br/>like a developer</b>
              </h1>
              <br/>
              <div style={{ maxWidth: "400px" }}>
                <h5 style={{ fontFamily: 'Roboto' }}>
                  Track leads, create invoices, and learn how to build landing pages just like this one!
                </h5>
              </div>

          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
